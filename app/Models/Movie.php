<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $fillable = [
        'title',
        'year',
        'poster',
        'plot',
        'director_id',
        'genre_id',
        'classification_id',
    ];

    public function director()
    {
        return $this->hasOne(Director::class, 'id');
    }

    public function genre()
    {
        return $this->hasOne(Genre::class, 'id');
    }

    public function classification()
    {
        return $this->hasOne(Classification::class, 'id');
    }

    public function actors()
    {
        return $this->belongsToMany(Actor::class, 'actors_movies', 'movie_id', 'actor_id');
    }
}
