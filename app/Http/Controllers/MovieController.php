<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use App\Repositories\Repository as Repo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class MovieController extends Controller
{
    protected $model;

    public function __construct(Movie $movie)
    {
        // set the model
        $this->model = new Repo($movie);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->model->with(['director', 'genre', 'classification', 'actors'])->all();
        return response()->json($data, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        DB::beginTransaction();
        try {

            // Validate the request
            $validate = Validator::make($request->toArray(), [
                'title'             => 'required',
                'year'              => 'required',
                'plot'              => 'required',
                'director_id'       => 'required',
                'genre_id'          => 'required',
                'classification_id' => 'required',
                'actors'            => 'required',
            ]);

            if ($validate->fails()) {
                $messages = $validate->messages();
                return response()->json(['message' => $messages], 400);
            }

            $data = $this->model->create($request->only($this->model->getModel()->fillable));

            $actors = [];
            foreach ($request->actors as $key => $value) {
                $actors[] += (int) $value;
            }

            $data->actors()->attach($actors);

            DB::commit();

            $data = $this->model->with(['director', 'genre', 'classification', 'actors'])->show($data->id);

            return response()->json(['data' => $data], 200);
        } catch (Exception $e) {
            DB::rollBack();
            // throw new \Exception($e->getMessage());
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->model->with(['director', 'genre', 'classification', 'actors'])->show($id);

        if (!$data) {
            return response()->json([
                'data'    => [],
                'message' => 'Not Found',
            ], 404);
        }

        return response()->json(['data' => $data], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {

            // Validate the request
            $validate = Validator::make($request->toArray(), [
                'title'             => 'required',
                'year'              => 'required',
                'plot'              => 'required',
                'director_id'       => 'required',
                'genre_id'          => 'required',
                'classification_id' => 'required',
            ]);

            if ($validate->fails()) {
                $messages = $validate->messages();
                return response()->json(['message' => $messages], 400);
            }

            $data = $this->model->update($request->only($this->model->getModel()->fillable), $id);

            if (!$data) {
                return response()->json([
                    'data'    => [],
                    'message' => 'Not Found',
                ], 404);
            }

            $actors = [];
            foreach ($request->actors as $key => $value) {
                $actors[] += (int) $value;
            }

            $data = $this->model->show($id);

            $data->actors()->sync($actors);

            DB::commit();

            $data = $this->model->with(['director', 'genre', 'classification', 'actors'])->show($data->id);

            return response()->json(['data' => $data], 200);
        } catch (Exception $e) {
            DB::rollBack();
            // throw new \Exception($e->getMessage());
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {

            $data = $this->model->show($id);

            $data->actors()->detach();

            $data->delete($id);

            if (!$data) {
                return response()->json([
                    'data'    => [],
                    'message' => 'Not Found',
                ], 404);
            }

            DB::commit();

            return response()->json([], 204);
        } catch (Exception $e) {
            DB::rollBack();
            // throw new \Exception($e->getMessage());
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }
}
