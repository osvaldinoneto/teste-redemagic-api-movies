<?php

Route::group(['prefix' => 'actors'], function () {
    Route::get('/', 'ActorController@index');
    Route::post('create', 'ActorController@store');
    Route::get('{id}', 'ActorController@show');
    Route::put('{id}', 'ActorController@update');
});

Route::group(['prefix' => 'classifications'], function () {
    Route::get('/', 'ClassificationController@index');
    Route::post('create', 'ClassificationController@store');
    Route::get('{id}', 'ClassificationController@show');
    Route::put('{id}', 'ClassificationController@update');
});

Route::group(['prefix' => 'directors'], function () {
    Route::get('/', 'DirectorController@index');
    Route::post('create', 'DirectorController@store');
    Route::get('{id}', 'DirectorController@show');
    Route::put('{id}', 'DirectorController@update');
});

Route::group(['prefix' => 'genres'], function () {
    Route::get('/', 'GenreController@index');
    Route::post('create', 'GenreController@store');
    Route::get('{id}', 'GenreController@show');
    Route::put('{id}', 'GenreController@update');
});

Route::group(['prefix' => 'movies'], function () {
    Route::get('/', 'MovieController@index');
    Route::post('create', 'MovieController@store');
    Route::get('{id}', 'MovieController@show');
    Route::put('{id}', 'MovieController@update');
    Route::delete('{id}', 'MovieController@destroy');
});
