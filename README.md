# Avaliação Rede Magic

Este projeto foi desenvolvido por mim para processo seletivo no que diz respeito a avaliação de conhecimento sobre desenvolvimento back-end. Para produção do teste foi estipulado o Framework Laravel.

# O Projeto

API REST.
Gerenciamento onde seja possível Criar, Listar, Editar, Visualizar e Remover.
Optei por usar Respository Pattern, de forma bem simplificada.
Utilizei a versão 6.x do Laravel devido a familiriadade como o mesmo, visto que ainda não tive contato com a versão 7.x.
A API consiste no gerenciamento de dados relacinados a filmes, tais como atores, diretores, classificaçao indicativa e gênero.
Não possui validações de forma mais detalhada, apenas em alguns poucos campos.

# Instalação do projeto

`
git clone https://osvaldinoneto@bitbucket.org/osvaldinoneto/teste-redemagic-api-movies.git
`

Acesse a pasta

`
cd teste-redemagic-api-movies
`

Faça a instalação das dependências através do composer:

`
composer install
`

Faça uma cópia do seu arquivo .env:

`
cp .env.example .env
`

Configure suas variáveis de ambiente para seu banco de dados:

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=pluri
DB_USERNAME=root
DB_PASSWORD=
```

Gere uma nova chave para APP_KEY:

`
php artisan key:generate
`

Se necessário, dê as permissões necessárias para permitir a escrita nas seguintes pastas:

`storage` e `bootstrap`

Exemplo:

`
chmod -R 775 storage
`

`
chmod -R 775 bootstrap
`

Após a criação do banco de dados que será utilizado na aplicação e setá-lo no .env como descrito acima, execute o seguinte comando para a criação das tabelas através das migrations:

`
php artisan migrate
`

Não foi criado seeds para este teste.

Para executar o projeto execute:

`
php artisan serve
`

# Postman

O arquivo com todas as rotas para teste da API no Postman econtra-se na pasta .github.
Utilizando o comando `php artisan serve`, a collection já está configurada com a rota gerada (http://127.0.0.1:8000).

`
[collection](.github/teste-redemagic-api-movies.postman_collection.json).
`


Made with much :purple_heart: and :muscle: by Osvaldino Neto :blush: <a href="https://www.linkedin.com/in/osvaldinoneto/">Talk to me!</a>